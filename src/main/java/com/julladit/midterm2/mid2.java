/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.midterm2;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class mid2 {

    public static int findSortedSubArray(int arr[], int n) {
        int i = 0, j = 0;
        int maxLength = 1, tempLength = 1;
        for (i = 0; i < n; i++) {
            tempLength = 1;
            for (j = i; j < n - 1; j++) {
                if (arr[j] <= arr[j + 1] && (j + 1) != n - 1) {
                    tempLength++;
                } else {
                    if (tempLength > maxLength) {
                        maxLength = tempLength;
                    }
                    break;
                }
            }
        }
        return maxLength;
    }

    public static void main(String[] args) {
        long start, stop;

        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = kb.nextInt();
        }
        System.out.println("Long Sorted Array is :  " + findSortedSubArray(arr, n));
        start = System.nanoTime();
        stop = System.nanoTime();
        System.out.println("time = " + (stop - start) * 1E-9 + "secs.");
    }
}
